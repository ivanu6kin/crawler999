@extends('layouts.account')

@section('content')
    <div class="">
        <h1 style="text-align: center">Posts Parsed filter: {{$posts->total()}}</h1>

        <div class="filter">

            <style>
                .setected_cats span {
                    border: 1px solid #dddddd;
                    padding: 6px;
                    display: inline-block;
                    margin: 2px;
                }
            </style>

            <form name="filter" method="post" action="filter">
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <div class="form-group">

                            <label for="exampleFormControlSelect2">
                                <h5>Categories</h5>
                            </label>
                            <select name="categories[]" multiple class="form-control" id="categories" size="20">
                                @foreach($categories as $category)
                                    <option
                                        {{in_array($category->id, $filterData['cats'])? 'selected' : ''}}
                                        value="{{ $category->id }}">
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>

                            <div class="setected_cats">
                                <h5>Выбранные</h5>
                                @foreach($categories as $category)
                                    @if(in_array($category->id, $filterData['cats']))
                                        <span>{{ $category->name }}</span>
                                    @endif
                                @endforeach
                            </div>

                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-3 mb-3">
                            <div class="form-group">

                                <label for="exampleFormControlSelect2">
                                    <h5>Regions</h5>
                                </label>
                                <select name="regions[]" multiple class="form-control" id="regions" size="20">
                                    @foreach($regions as $region)
                                        <option
                                            {{in_array($region['id'], $filterData['regions'])? 'selected' : ''}}
                                            value="{{ $region['id'] }}">
                                            {{ $region['name'] }}
                                        </option>
                                    @endforeach
                                </select>

                                <div class="setected_cats">
                                    <h5>Выбранные</h5>
                                    @foreach($regions as $region)
                                        @if(in_array($region['id'], $filterData['regions']))
                                            <span>{{ $region['name'] }}</span>
                                        @endif
                                    @endforeach
                                </div>

                            </div>
                        </div>
                    <div class="col-md-3 mb-3">
                        <div class="form-group">
                            <label for="exampleFormControlSelect2">Example multiple select</label>
                            <select multiple class="form-control" id="exampleFormControlSelect2">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 mb-3">
                        <div class="form-group">
                            <label for="exampleFormControlSelect2">Example multiple select</label>
                            <select multiple class="form-control" id="exampleFormControlSelect2">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 mb-3">
                        <div class="form-group">
                            <label for="exampleFormControlSelect2">Example multiple select</label>
                            <select multiple class="form-control" id="exampleFormControlSelect2">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>
                </div>

                <button class="btn btn-primary" type="submit">Submit</button>
            </form>
        </div>

        {{ $posts->links() }}

        <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>Ad id</th>
                <th>Title</th>
                <th>Description</th>
                <th>Type</th>
                <th>Price</th>
                <th>MDL</th>
                <th>EURO</th>
                <th>USD</th>
                <th>author</th>
                <th>Photos</th>
                <th>duplicate</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($posts as $post)
                <tr>
                    <td>{{ $post->ad_id }}</td>
                    <td>{{ $post->title }}</td>
                    <td style="font-size: 15px">{{ $post->text }}</td>
                    <td>{{ $post->type }}</td>
                    <td>{{ $post->price }}</td>
                    <td>{{ $post->mdl }} mdl</td>
                    <td>{{ $post->euro }} euro</td>
                    <td>{{ $post->usd }} usd</td>
                    <td>{{ $post->author }}</td>
                    <td>

                        {{--{{json_decode($post->photos)}}--}}

{{--                        {{dd($post->photos)}}--}}
{{--                        {{dd($keywords = preg_split("/[\s,]+/", $post->photos))}}--}}
                        {{--@foreach(json_decode($post->photos) as $photo)--}}

                            {{--{{$photo}}--}}
                            {{--<img width="50" src="{{$photo}}" alt="img">--}}
                            {{--{{$photo}}--}}
                        {{--@endforeach--}}

                        {{--{{ dd($post->photos) }}--}}
                        {{--@if(!empty($post->photos))--}}
                            {{--@fore--}}
                            {{--{{ $post->photos }} author--}}
                        {{--@endif--}}
                    </td>
                    <td>{{ $post->duplicate? 'yes' : 'no' }}</td>
                    {{--<td>--}}
                        {{--@if(!empty($post->image))--}}
                            {{--<img src="https://i.simpalsmedia.com/999.md/BoardImages/160x120/{{ $post->image }}" alt="preview">--}}
                        {{--@endif--}}
                    {{--</td>--}}
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $posts->links() }}
    </div>
@endsection
