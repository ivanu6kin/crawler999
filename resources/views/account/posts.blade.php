@extends('layouts.mini')
@section('content')
    <style>
        p {
            word-break: break-word;
        }
        .filter_tags {

        }

        .filter_tags li {
            display: inline-block;
            margin: 0;
            border: 1px solid #e3e3e3;
            margin-left: 7px;
            margin-bottom: 5px;
            padding: 10px;
            border-radius: 10px;
            background: #ededed;
            cursor: pointer;
        }
        .filter_tags li:hover {
            opacity: 0.8;
        }

        .post_image {
            width: 100px;
            height: auto;
            display: inline-block;
            margin-bottom: 3px;
            margin-right: 3px;
        }

        /*loader*/
        .loader {
            position: fixed;
            width: 100%;
            height: 100%;
            background: #47474769;
        }
        .lds-hourglass {
            position: relative;
            width: 80px;
            height: 80px;
            margin: 0 auto;
            display: block;
            top: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
        }
        .lds-hourglass:after {
            content: " ";
            display: block;
            border-radius: 50%;
            width: 0;
            height: 0;
            margin: 8px;
            box-sizing: border-box;
            border: 32px solid #fff;
            border-color: #fff transparent #fff transparent;
            animation: lds-hourglass 1.2s infinite;
        }
        @keyframes lds-hourglass {
            0% {
                transform: rotate(0);
                animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
            }
            50% {
                transform: rotate(900deg);
                animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
            }
            100% {
                transform: rotate(1800deg);
            }
        }
        /*loader end*/
    </style>

    <div id="filter">
        <div v-if="is_loading" class="loader">
            <div class="lds-hourglass"></div>
        </div>
        <h2>Filter:</h2>
        <div>
            <div class="form-group">
                total: [ pagination.total ]
            </div>
            <div class="form-group">
                Last Page: [ pagination.last_page ]
            </div>
            <div class="form-group">
                Page: <input type="number" @input="paginationChange" v-model="pagination.current">
            </div>
            <div class="form-group">
                <div v-on:click="submit" class="btn btn-primary">Submit</div>
            </div>
        </div>
        <ul class="filter_tags">
            <li
                v-for="(value, key) in filter_tags"
                @click="deleteFilterTag(key)"
            >
                [ showTag(value) ]
            </li>
        </ul>
        <div class="left-block">
            <form>
                <div class="form-group">
                    <label>Region: </label>
                    <input
                        type="text"
                        v-model="filter_regions"
                        @input="filterRegions"
                        placeholder="буюканы"
                        class="form-control"
                    >
                    <ul class="regions">
                        <li v-for="region in regions"  @click="addRegionToFilterTags(region)">
                            [ region.name ]
                        </li>
                    </ul>
                </div>
                <div class="form-group">
                    <label>Spec: </label>
                    <input
                        type="text"
                        v-model="filter_spec"
                        @input="filterSpecs"
                        placeholder="Высота потолков"
                        class="form-control"
                    >
                    <ul class="regions">
                        <li v-for="spec in specs"  @click="addSpecToFilterTags(spec)">
                            [ spec.key ]=[ spec.value ]
                        </li>
                    </ul>
                </div>
                <div class="form-group">
                    <label>Недвижимость: </label>
                    <ul class="regions">
                        <li v-for="cat in cats"  @click="addCatToFilterTags(cat)">
                            [ cat.name ]
                        </li>
                    </ul>
                </div>
                <div class="form-group">
                    <label>Заголовок обьявления: </label>
                    <input
                        type="text"
                        v-model="filter_title"
                        placeholder="*title*"
                        class="form-control"
                    >
                </div>
                <div class="form-group">
                    <label>Тело обьявления: </label>
                    <input
                        type="text"
                        v-model="filter_desc"
                        placeholder="*desc*"
                        class="form-control"
                    >
                </div>
                <div class="form-group">
                    <label>Region part: </label>
                    <input
                        type="text"
                        v-model="filter_region_part"
                        placeholder="*буюканы*"
                        class="form-control"
                    >
                </div>
                <div class="form-group">
                    <label>Id обьявления: </label>
                    <input
                        type="text"
                        v-model="filter_ad_id"
                        placeholder="ad_id"
                        class="form-control"
                    >
                </div>
                <div class="form-group">
                    <label>Money Type: </label>
                    <select class="form-control" v-model="filter_money_type">
                        <option value="">не выбрано</option>
                        <option value="mdl">mdl</option>
                        <option value="usd">usd</option>
                        <option value="euro">euro</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>> Money <</label>
                    <input
                        type="text"
                        v-model="filter_money_more"
                        placeholder="100"
                        class="form-control"
                    >
                    <input
                        type="text"
                        v-model="filter_money_less"
                        placeholder="200"
                        class="form-control"
                    >
                </div>
                <div class="form-group">
                    <label>Тип обьявления: </label>
                    <select class="form-control" v-model="filter_sell_type">
                        <option value="">не выбрано</option>
                        <option value="ищу услугу">ищу услугу</option>
                        <option value="куплю">куплю</option>
                        <option value="меняю">меняю</option>
                        <option value="предлагаю услугу">предлагаю услугу</option>
                        <option value="продам">продам</option>
                        <option value="сдаю">сдаю</option>
                        <option value="сдаю помесячно">сдаю помесячно</option>
                        <option value="сдаю посуточно">сдаю посуточно</option>
                        <option value="тип: сдаю посуточно">тип: сдаю посуточно</option>
                        <option value="сниму">сниму</option>
                    </select>
                </div>
                <div class="form-group">
                    <div v-on:click="submit" class="btn btn-primary">Submit</div>
                </div>
            </form>
        </div>
        <div class="content-block">
            <table id="table_posts" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Description</th>
                    <th>Photos</th>
                </tr>
            </thead>
                <tbody>
                    <tr v-for="post in posts">
                        <td>
                            <ul>
                                <li><b>Ad_id: </b>[ post.ad_id ]</li>
                                <li><b>author: </b>[ post.author ]</li>
                                <li><b>Type: </b>[ post.type ]</li>
                                <li><b>Phones: </b>[ post.phones ]</li>
                                <li><b>Price: </b>[ post.price ]</li>
                                <li><b>mdl: </b>[ post.mdl ]</li>
                                <li><b>euro: </b>[ post.euro ]</li>
                                <li><b>usd: </b>[ post.usd ]</li>
                                <li><b>duplicate: </b>[ post.duplicate ]</li>
                            </ul>
                        </td>
                        <td style="font-size: 15px">
                            <h5>[ post.title ]</h5>
                            <br />
                            <p>[ post.text ]</p>
                        </td>
                        <td>
                            <img
                                v-for="photo in post.parsed_photos"
                                class="post_image"
                                :src="getPhotoUrl(photo.path)"
                                alt="image"
                            />
                            <span v-for="(photo, key) in getParsed999PhotoUrls(post.photos)">
                                <a target="_blank" :href="[ photo ]">link [key]</a>,
                            </span>
{{--                            [ post.photos | jsonPretty ]--}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
