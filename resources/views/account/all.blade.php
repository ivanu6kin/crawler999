@extends('layouts.account')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Posts : {{$posts->total()}}</h1>

        {{ $posts->links() }}

        <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr>
                {{--<th>Ad id</th>--}}
                {{--<th>Title</th>--}}
                {{--<th>Description</th>--}}
                {{--<th>Type</th>--}}
                {{--<th>Price</th>--}}
                {{--<th>MDL</th>--}}
                {{--<th>EURO</th>--}}
                {{--<th>USD</th>--}}
                {{--<th>author</th>--}}
                {{--<th>Photos</th>--}}
                {{--<th>duplicate</th>--}}

                <th>Ad id</th>
                <th>Title</th>
                <th>image</th>
                <th>Cat Path</th>
                <th>Raw</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($posts as $post)
                <tr>
                    {{--<td>{{ $post->ad_id }}</td>--}}
                    {{--<td>{{ $post->title }}</td>--}}
                    {{--<td>{{ $post->text }}</td>--}}
                    {{--<td>{{ $post->type }}</td>--}}
                    {{--<td>{{ $post->price }}</td>--}}
                    {{--<td>{{ $post->mdl }} mdl</td>--}}
                    {{--<td>{{ $post->euro }} euro</td>--}}
                    {{--<td>{{ $post->usd }} usd</td>--}}
                    {{--<td>{{ $post->author }}</td>--}}
                    {{--<td>{{ $post->photos }} author</td>--}}
                    {{--<td>{{ $post->duplicate? 'yes' : 'no' }}</td>--}}

                    <td>{{ $post->ad_id }}</td>
                    <td>{{ $post->title }}</td>
                    <td>
                        @if(!empty($post->image))
                            <img src="https://i.simpalsmedia.com/999.md/BoardImages/160x120/{{ $post->image }}" alt="preview">
                        @endif
                    </td>
                    <td>{{ $post->sub_cat_path }}</td>
                    <td>{{ !empty($post->raw)? 'yes' : 'no' }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $posts->links() }}
    </div>
@endsection
