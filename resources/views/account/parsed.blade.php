@extends('layouts.account')

@section('content')
    <div class="">
        <h1 style="text-align: center">Posts Parsed : {{$posts->total()}}</h1>

        {{ $posts->links() }}

        <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>Ad id</th>
                <th>Title</th>
                <th>Description</th>
                <th>Type</th>
                <th>Price</th>
                <th>MDL</th>
                <th>EURO</th>
                <th>USD</th>
                <th>author</th>
                <th>Photos</th>
                <th>duplicate</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($posts as $post)
                <tr>
                    <td>{{ $post->ad_id }}</td>
                    <td>{{ $post->title }}</td>
                    <td style="font-size: 15px">{{ $post->text }}</td>
                    <td>{{ $post->type }}</td>
                    <td>{{ $post->price }}</td>
                    <td>{{ $post->mdl }} mdl</td>
                    <td>{{ $post->euro }} euro</td>
                    <td>{{ $post->usd }} usd</td>
                    <td>{{ $post->author }}</td>
                    <td>

                        {{--{{json_decode($post->photos)}}--}}

{{--                        {{dd($post->photos)}}--}}
{{--                        {{dd($keywords = preg_split("/[\s,]+/", $post->photos))}}--}}
                        {{--@foreach(json_decode($post->photos) as $photo)--}}

                            {{--{{$photo}}--}}
                            {{--<img width="50" src="{{$photo}}" alt="img">--}}
                            {{--{{$photo}}--}}
                        {{--@endforeach--}}

                        {{--{{ dd($post->photos) }}--}}
                        {{--@if(!empty($post->photos))--}}
                            {{--@fore--}}
                            {{--{{ $post->photos }} author--}}
                        {{--@endif--}}
                    </td>
                    <td>{{ $post->duplicate? 'yes' : 'no' }}</td>
                    {{--<td>--}}
                        {{--@if(!empty($post->image))--}}
                            {{--<img src="https://i.simpalsmedia.com/999.md/BoardImages/160x120/{{ $post->image }}" alt="preview">--}}
                        {{--@endif--}}
                    {{--</td>--}}
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $posts->links() }}
    </div>
@endsection
