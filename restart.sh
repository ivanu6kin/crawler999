#!/bin/bash

# Check if MySQL is running
sudo service mysql status > /dev/null 2>&1

# Restart the MySQL service if it's not running.
UP=$(pgrep mysql | wc -l);
if [ "$UP" -ne 1 ];
then
  date >> /var/www/html/restart-server-log.txt
  sudo reboot
else
  echo "All is well." >> /var/www/html/restart-server-log.txt
fi
