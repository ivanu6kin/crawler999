<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostRegion extends Model
{
    protected $fillable = ['post_id','region_id'];
}
