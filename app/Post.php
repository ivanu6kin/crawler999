<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    const WAS_PARSED_NOT_VALID = -10;
    const WAS_PARSED_ERROR = -1;
    const WAS_PARSED_SUCCESS = 1;

    protected $attributes = [
        'raw' => '',
    ];
}
