<?php

namespace App\Jobs;

use App\Parser\ParsePage;
use App\Parser\ParsePageImages;
use App\Post;
use App\PostParsedNew;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use \Exception;
use Illuminate\Support\Facades\Log;

class ProcessParsePost implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $post;

    public function __construct(Post $post)
    {
        $this->queue = 'parse.post';
        $this->post = $post;
    }
    public function handle()
    {
        $parse = new ParsePage($this->post, false);
        $postParsedNew = $parse->parseHtml();

        /** @var ParsePageImages $service */
        $service = resolve(ParsePageImages::class);
        $service->saveImages($postParsedNew);

        if($postParsedNew instanceof Exception) {
            throw new Exception(sprintf('process parse post error id %s', $postParsedNew->id));
        }
    }
}
