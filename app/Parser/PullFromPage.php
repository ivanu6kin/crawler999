<?php namespace App\Parser;

use App\Jobs\ProcessParsePost;
use App\Post;
use App\Pull;
use Carbon\Carbon;
use \Exception;
use Symfony\Component\DomCrawler\Crawler;

class PullFromPage {
    /**
     * @var ParserClient $client
     */
    private $client;

    private $url = 'https://999.md/ru/category/real-estate';

    public static $crawlerFilter = [
        'ads_item' => '.ads-list-photo-item',
        'ad_title' => '.ads-list-photo-item-title',
    ];

    function __construct()
    {
        $this->client = resolve(ParserClient::class);
        $this->client->setWithProxy(false);
    }

    public function getPull()
    {
        $request = $this->client->get($this->url);
        $code = $request->getStatusCode();
        $response = $request->getBody();
        $html = $response->getContents();
        $crawler = new Crawler($html);

        $ads_list = $crawler->filter(self::$crawlerFilter['ads_item'])->each(function (Crawler $node, $i)
        {
            $href = $node->filter('a')->attr('href');
            $ad_id = (int)str_replace('/ru/', '', $href);
            $title = trim($node->filter(self::$crawlerFilter['ads_item'])->text());

            return [
                'ad_id' => $ad_id,
                'href' => $href,
                'title' => $title,
            ];
        });

        $pull = Pull::create();
        try {
            $pull->code = $code;
            $pull->raw = count($ads_list)? json_encode($ads_list) : json_encode($html);
            $pull->reseted = 0;
        } catch (Exception $e) {
            $pull->raw = $e->getMessage();
        }
        $pull->save();

        $this->parsePull($pull);
    }

    private function parsePull(Pull $pull)
    {
        try {
            $rawArr = json_decode($pull->raw, true);
            $rawArrKeyAdId = [];
            //set ad_id as key
            foreach ($rawArr as $itemK => $itemV) {
                $rawArrKeyAdId[$itemV['ad_id']] = $itemV;
            }

            $ads_ad_id_list = array_unique(array_filter(array_keys($rawArrKeyAdId)));

            /**
             * Найти дубликаты за сегодня
             */
            $duplicateDbList = Post::select('ad_id')
                ->whereBetween('created_at', [
                    Carbon::now()->startOfDay()->toDateTimeString(),
                    Carbon::now()->endOfDay()->toDateTimeString()
                ])
                ->whereIn('ad_id', $ads_ad_id_list)->get()->toArray();
            $duplicateDbList = array_column($duplicateDbList, 'ad_id');

            /**
             * Найти обьявления которых нет в базе
             */
            $not_in_db_ads_list = [];
            foreach ($ads_ad_id_list as $ad) {
                if(!in_array($ad, $duplicateDbList)) {
                    $not_in_db_ads_list[] = $ad;
                }
            }

            foreach ($not_in_db_ads_list as $item)
            {
                if(!array_key_exists($item, $rawArrKeyAdId)) {
                    continue;
                }

                $ad = $rawArrKeyAdId[$item];
                $post = Post::create();
                $post->pull_id = !empty($pull->id)? $pull->id : 0;
                $post->cat = '';
                $post->sub_cat = '';
                $post->cat_path = '';
                $post->sub_cat_path = '';
                $post->image = '';
                $post->title = !empty($ad['title'])? $ad['title'] : '';
                $post->_id = !empty($ad['ad_id'])? $ad['ad_id'] : 0;
                $post->ad_id = !empty($ad['ad_id'])? $ad['ad_id'] : '';
                $post->type = 'standard';
                $post->save();
                ProcessParsePost::dispatch($post);
            }

            //parse pull
            $pull->next = 1;
            $pull->save();

            return true;

        } catch (Exception $e) {
            return $e;
        }
    }
}
