<?php namespace App\Parser;

use App\Proxy;
use GuzzleHttp\Client;

class ParserClient {

    protected $client;
    protected $agent;

    public $proxy;
    public $withProxy = true;

    public $userAgents = [
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
    ];

    function __construct(Client $client)
    {
        $this->agent = $this->userAgents[array_rand($this->userAgents)];
        $this->proxy = Proxy::getActiveRandProxy();
        $this->client = $client;
    }

    public function setWithProxy($proxy = true)
    {
        $this->withProxy = $proxy;
    }

    public function getProxy ()
    {
        return $this->proxy;
    }

    //get pull
    public function get($url = '')
    {
        if(!$url)return false;

        $params = [
            'timeout' => 15, // Response timeout
            'connect_timeout' => 15, // Connection timeout
            'headers' => [
                'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
                'Accept-Encoding' => 'gzip, deflate, br',
                'Accept-Language' => 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,bg;q=0.6,ro;q=0.5',
                'Cache-Control' => 'no-cache',
                'Connection' => 'keep-alive',
                'Host' => '999.md',
                'Pragma' => 'no-cache',
                'Upgrade-Insecure-Requests' => 1,
                'Referer' => 'https://999.md/ru/',
                'User-Agent' => $this->agent,
            ]
        ];

//        if($this->withProxy)
//        {
//            $params['proxy'] = sprintf('%s:%s', $this->proxy->ip, $this->proxy->port);
//        }

        return $this->client->get($url, $params);
    }
}
