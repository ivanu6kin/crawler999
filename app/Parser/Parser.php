<?php namespace App\Parser;

use App\Jobs\ProcessParsePost;
use App\Post;
use App\Pull;
use Carbon\Carbon;
use GuzzleHttp\Client;
use \Exception;

class Parser {

    private $client;

    private $url = 'https://999.md/services/lastads';

    function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getPull()
    {
        $lastPull = Pull::where('reseted','!=', '')->orderBy('id', 'DESC')->first();
        $params = [
            'categoryId' => 270,
            'lang' => 'ru',
        ];

        if ($lastPull instanceof Pull) {
            $params['timestamp'] = $lastPull->reseted;
        }

        try {
            $pull = Pull::create();
            $urlFull = sprintf('%s/?%s', $this->url, http_build_query($params));
            $request = $this->client->get($urlFull);
            $code = $request->getStatusCode();
            $response = $request->getBody();
            $json = $response->getContents();
            $items = json_decode($json, true);

            if(!empty($items))
            {
                $pull->reseted = $items[0]['reseted'];
            }else{
                if(!empty($params['timestamp']))$pull->reseted = $params['timestamp'];
            }

            $pull->code = $code;
            $pull->raw = $json;
        } catch (Exception $e) {
           $pull->raw = $e->getMessage();
        }

        $pull->save();

        //parse
        $this->parsePull($pull);
    }

    public function test(){
//        $startOfDay = Carbon::now()->startOfDay()->toDateTimeString();
//        $endOfDay = Carbon::now()->endOfDay()->toDateTimeString();
//        $duplicatePost = Post::whereBetween('created_at', [$startOfDay, $endOfDay])->first();
    }

    private function parsePull(Pull $pull)
    {
        try {
            $rawArr = json_decode($pull->raw, true);

//            $startOfDay = Carbon::now()->startOfDay()->toDateTimeString();
//            $endOfDay = Carbon::now()->endOfDay()->toDateTimeString();
//            $duplicatePost = Post::whereBetween('created_at', [$startOfDay, $endOfDay])->first();
//            \Log::info(sprintf('%s | %s | %s', $startOfDay, $endOfDay, $duplicatePost instanceof Post));
            // Carbon\Carbon::parse('11/06/1990')->format('d/m/Y')

            foreach ($rawArr as $item)
            {
                /**
                 * Найти дубликат и исключить
                 */
                if (!empty($item['_id'])) {
                    $duplicatePost = Post::select('id')
                        ->whereBetween('created_at', [
                            Carbon::now()->startOfDay()->toDateTimeString(),
                            Carbon::now()->endOfDay()->toDateTimeString()
                        ])
                        ->where('_id', $item['_id'])
                        ->first();

                    if ($duplicatePost instanceof Post) {
                        continue;
                    }
                }

                $post = Post::create();
                $post->pull_id = !empty($pull->id)? $pull->id : 0;
                $post->cat = !empty($item['categories']['category']['title'])? $item['categories']['category']['title'] : '';
                $post->sub_cat = !empty($item['categories']['subcategory']['title'])? $item['categories']['subcategory']['title'] : '';
                $post->cat_path = !empty($item['categories']['category']['url'])? $item['categories']['category']['url'] : '';
                $post->sub_cat_path = !empty($item['categories']['subcategory']['url'])? $item['categories']['subcategory']['url'] : '';
                $post->image = !empty($item['image'])? $item['image'] : '';
                $post->title = !empty($item['title'])? $item['title'] : '';
                $post->_id = !empty($item['_id'])? $item['_id'] : 0;
                $post->ad_id = !empty($item['ad_id'])? $item['ad_id'] : '';
                $post->type = !empty($item['type'])? $item['type'] : '';
                $post->save();

                //setjob
                #$this->setInJobPost($post);
                //exec("/usr/local/bin/php /var/www/html/artisan parse:post {$post->id} > /dev/null &");
            }

            //parse pull
            $pull->next = 1;
            $pull->save();

            return true;

        } catch (Exception $e) {
            return $e;
        }
    }

    public function setInJobPost(Post $post)
    {
        try {
            return ProcessParsePost::dispatch($post);
        } catch (Exception $e)
        {
//            return $e;
        }
    }
}
