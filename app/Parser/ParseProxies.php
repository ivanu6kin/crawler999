<?php namespace App\Parser;

use App\Proxy;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;
use \Exception;
use \Log;

class ParseProxies {

    protected $client;

    function __construct()
    {
        $this->client = new Client();
    }

    public function getPageAndSave()
    {
//        $url = sprintf('https://hidemyna.me/en/proxy-list/?start=%s#list', $start);
//        $url = sprintf('https://hidemyna.me/en/proxy-list/?start=%s', $start);
        $url = sprintf('https://free-proxy-list.net/');

        $request = $this->client->get($url,
            [
                'headers' => [
//                    ':authority' => 'hidemyna.me',
//                    ':method' => 'GET',
//                    ':path' => '/en/proxy-list/?start=' . $start,
//                    ':scheme' => 'https',
//                    'accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
//                    'accept-encoding' => 'gzip, deflate, br',
//                    'accept-language' => 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,bg;q=0.6,ro;q=0.5',
//                    'cache-control' => 'no-cache',
//                    'cookie' => '__cfduid=d8cf7907509a24222ef7da054dbe9cc3c1559813293; cf_clearance=b18c381117f9996f732fbc3dda2dee6145c3f070-1559813298-86400-150; t=122372445; jv_enter_ts_EBSrukxUuA=1559813306510; jv_visits_count_EBSrukxUuA=1; jv_refer_EBSrukxUuA=https%3A%2F%2Fhidemyna.me%2Fen%2Fproxy-list%2F; PHPSESSID=g1eav685hs9s4pvh1fkovb46g2; jv_pages_count_EBSrukxUuA=18',
//                    'content-type' => 'text/html; charset=iso-8859-1',
//                    'pragma' => 'no-cache',
//                    'Pragma' => 'no-cache',
//                    'upgrade-insecure-requests' => 1,
                    'user-agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
                ]
            ]);

        $code = $request->getStatusCode();
        $response = $request->getBody();
        $html = $response->getContents();

        $crawler = new Crawler($html);

        $proxies = $crawler->filter('#proxylisttable tr')->each(function (Crawler $node, $i)
        {
            $tds = $node->filter('td')->each(function (Crawler $node, $i)
            {
                return $node->text();
            });

            return $tds;
        });

        foreach ($proxies as $proxy)
        {
            $ip = !empty($proxy[0])? trim($proxy[0]) : false;
            $port = !empty($proxy[1])? (int)trim($proxy[1]) : false;
            $region = !empty($proxy[3])? $proxy[3] : false;

            if($ip and $port and $region){
                try {

                    if(!$findProxy = Proxy::whereIp($ip)->first())
                    {
                        Proxy::create(['ip' => $ip, 'port' => $port, 'region' => $region]);
                    }

                } catch (Exception $e)
                {
                    Log::info($e->getMessage());
                }
            }
        }

        return $html;
    }
}