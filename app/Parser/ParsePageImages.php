<?php namespace App\Parser;

use App\PostParsedNew;
use App\PostParsedPhotos;
use Carbon\Carbon;
use Image;

class ParsePageImages {

    private $path;

    function __construct()
    {
        $this->path = sprintf('%s/%s', public_path(), '999');
    }

    private function createFolders(PostParsedNew $postParsedNew): string
    {
        $created_date = Carbon::parse($postParsedNew->created_at);
        $year_folder_path = sprintf('%s/%s', $this->path, $created_date->year);
        if (!file_exists($year_folder_path)) {
            mkdir($year_folder_path, 0777, true);
        }

        $month_folder_path = sprintf('%s/%s', $year_folder_path, $created_date->month);
        if (!file_exists($month_folder_path)) {
            mkdir($month_folder_path, 0777, true);
        }

        $day_folder_path = sprintf('%s/%s', $month_folder_path, $created_date->day);
        if (!file_exists($day_folder_path)) {
            mkdir($day_folder_path, 0777, true);
        }

        $ad_folder_path = sprintf('%s/%s', $day_folder_path, $postParsedNew->ad_id);
        if (!file_exists($ad_folder_path)) {
            mkdir($ad_folder_path, 0777, true);
        }

        return $ad_folder_path;
    }

    public function saveImages(PostParsedNew $postParsedNew): void
    {
        if($postParsedNew->is_parsed_photos) {
            return;
        }

        $photos = array_slice($postParsedNew->photos, 0, 3);
        if(empty($photos)) {
            $postParsedNew->is_parsed_photos = 1;
            $postParsedNew->save();
            return;
        }

        $hasImage = PostParsedPhotos::where('ad_id', '=', $postParsedNew->ad_id)->first();
        if($hasImage) {
            $postParsedNew->is_parsed_photos = 1;
            $postParsedNew->save();
            return;
        }

        $absolutePath = $this->createFolders($postParsedNew);
        $relativePath = str_replace($this->path, '', $absolutePath);
        $i = 0;
        foreach ($photos as $photoUrl) {
            $image = Image::make($photoUrl)->resize(200, 200);
            $imageName = sprintf('%s[%s].%s', $postParsedNew->ad_id, $i, 'jpg');
            $absoluteImagePath = sprintf('%s/%s', $absolutePath, $imageName);
            $relativeImagePath = sprintf('%s/%s', $relativePath, $imageName);
            $image->save($absoluteImagePath, 60);
            PostParsedPhotos::create(['ad_id' => $postParsedNew->ad_id, 'path' => $relativeImagePath]);
            $i++;
        }

        $postParsedNew->is_parsed_photos = 1;
        $postParsedNew->save();
    }
}
