<?php namespace App\Parser;

use App\Category;
use App\Error;
use App\Parser\Exceptions\NotValidException;
use App\Parser\Exceptions\ParseException;
use App\Post;
use App\PostCategory;
use App\PostParsedNew;
use App\PostRegion;
use App\RegExp\RegExp;
use App\Region;
use App\Spec;
use Symfony\Component\DomCrawler\Crawler;
use \Exception;
use \Log;

class ParsePage {

    protected $client;
    protected $crawler;
    public $post;
    public $url = 'http://999.md/ru';
    public $data = [];
    public $insertPostCats = [];
    public $insertPostRegions = [];
    public $insertPostSpecs = [];
    //найти в цене договорная
    #public $priceContract = false;

    public static $crawlerReplace = [
        'type' => ['тип:'],
    ];

    public static $crawlerFilter = [
        'title' => 'title',
        'text' => 'body .adPage__content__description',
        'type' => 'body .adPage__aside__stats__type',
        'shows' => 'body .adPage__aside__stats__views',
        'phone' => 'body .adPage__content__phone',
        'region' => 'body .adPage__content__region',
        'price' => 'body .adPage__content__price-feature__prices',
        'authorName' => 'body .adPage__aside__stats__owner__login',
        'authorPath' => 'body .adPage__aside__stats__owner__login',
        'date' => 'body .adPage__aside__stats__date',
        'categoriesRemoveRoot' => 'body .breadcrumbs li.root a',
        'categories' => 'body .breadcrumbs li > a',
        'photos' => 'body a.mfp-image img',
        'specs' => 'body .adPage__content__features__col',
    ];

    private $raw;

    /**
     * @param Post $post
     * @param bool $proxy
     */
    function __construct(Post $post, $proxy = true)
    {
        $this->client = resolve(ParserClient::class);
        $this->client->setWithProxy($proxy);
        $this->post = $post;
        $this->getPage();
        $this->crawler = new Crawler($this->raw);
    }

    public function getPage()
    {
        if (!$this->raw) {
            try {
                $url = sprintf('%s/%s', $this->url, $this->post->ad_id);
                $request = $this->client->get($url);
                $code = $request->getStatusCode();
                $response = $request->getBody();
                $html = $response->getContents();
                $pos = strripos($html, 'HTTP/1.1 400 Bad Request');

                if ($pos !== false) {
                    throw new Exception('HTTP/1.1 400 Bad Request');
                }

                $this->raw = $html;
                $this->post->request_code = $code;
                //Exception нужно будет убрать т.к через очередь будет повтор
            } catch (Exception $e) {
                $this->post->was_parsed = -1;
                Error::log($e, ['post_id' => $this->post->id, 'ad_id' => $this->post->_id]);
            }
            $this->post->save();
        }
    }

    public function title()
    {
        $element = trim($this->crawler->filter(self::$crawlerFilter['title'])->first()->text());
        $this->data['title'] = $element;
        if(empty($element)) throw new ParseException('empty title');
    }

    public function text()
    {
        try {
            $element = trim($this->crawler->filter(self::$crawlerFilter['text'])->first()->text());
            $this->data['text'] = $element;
            $this->data['searchText'] = mb_substr(RegExp::searchText($element),0, 200);
        }catch (Exception $e) {}
    }

    public function type()
    {
        $element = trim(mb_strtolower($this->crawler->filter(self::$crawlerFilter['type'])->first()->text()));
        $this->data['type'] = mb_strtolower($element);

        foreach (self::$crawlerReplace['type'] as $search) {
            $this->data['type'] = trim(str_replace($search, '', $this->data['type']));
        }

        if(empty($element)) throw new ParseException('empty type');
    }

    public function shows()
    {
        $element = trim(mb_strtolower($this->crawler->filter(self::$crawlerFilter['shows'])->first()->text()));
        $this->data['shows'] = (int)RegExp::onlyInts(RegExp::getOnlyFirstInt($element));
        if(!$element && $element != '0') throw new ParseException('empty shows');
    }

    public function phone()
    {
        try {
            $element = trim($this->crawler->filter(self::$crawlerFilter['phone'])->first()->text());
            $this->data['phone'] = RegExp::replaceSpaces($element);
        } catch (Exception $e) {}
    }

    public function region()
    {
        $element = trim($this->crawler->filter(self::$crawlerFilter['region'])->first()->text());
        $regions = array_map('trim', explode(',', mb_strtolower(str_replace("Регион:", "", $element))));
        $this->data['region'] = $element;
        $this->data['regionParsed'] = $regions;
        if(empty($element)) throw new ParseException('empty region');
    }

    public function price()
    {
        $element = mb_strtolower(trim($this->crawler->filter(self::$crawlerFilter['price'])->first()->text()));
        $this->data['price'] = RegExp::replaceSpaces($element);

        $isContactPrice = preg_match('/договорная/i', $element);

        if ($isContactPrice) {
            return;
        }

        if (empty($element)) {
            throw new ParseException('empty price');
        }

        #EURO
        $euro = trim(RegExp::getEuro($element));
        if ($euro) {
            $this->data['euro'] = RegExp::onlyMoney($euro);
        };

        //DOLLAR
        $dollar = trim(RegExp::getDollars($element));
        if ($dollar) {
            $this->data['usd'] = RegExp::onlyMoney($dollar);
        }

        //MDL
        $mdl = trim(RegExp::getMDL($element));
        if ($mdl) {
            $this->data['mdl'] = RegExp::onlyMoney($mdl);
        }

        if(!$euro && !$dollar && !$mdl) {
            throw new ParseException('empty mdl,usd,euro');
        }
    }

    public function authorName()
    {
        try {
            $element = trim(mb_strtolower($this->crawler->filter(self::$crawlerFilter['authorName'])->first()->text()));
            $this->data['authorName'] = $element;
        } catch (Exception $e) {}
    }

    public function authorPath()
    {
        try {
            $element = trim($this->crawler->filter(self::$crawlerFilter['authorPath'])->first()->attr('href'));
            $this->data['authorPath'] = $element;
        } catch (Exception $e) {}
    }

    public function date()
    {
        $element = mb_strtolower(trim($this->crawler->filter(self::$crawlerFilter['date'])->first()->text()));
        $this->data['date'] = $element;
        if(empty($element)) throw new ParseException('empty date');
//        try {
//            $ru_month = array( 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря' );
//            $ru_month1 = array( 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь' );
//            $ru_monthMini = array( 'янв.', 'фев.', 'мар.', 'апр.', 'май', 'июн.', 'июл.', 'авг.', 'сен.', 'окт.', 'ноя.', 'дек.' );
//            $ru_monthMiniWithoutPoint = array( 'янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек' );
//            $integer_month = array( '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12' );
//            $dataArr = explode(',',$element);
//            $dateHtml = $dataArr[0];
//            $dataArr = explode(' ', $dateHtml);
//            $dateD = (int)$dataArr[0];
//            $dateMon = $dataArr[1];
//            $dateMon = str_replace($ru_month, $integer_month, $dateMon);
//            $dateMon = str_replace($ru_month1, $integer_month, $dateMon);
//            $dateMon = str_replace($ru_monthMini, $integer_month, $dateMon);
//            $dateMon = str_replace($ru_monthMiniWithoutPoint, $integer_month, $dateMon);
//            $dateMon = (int)$dateMon;
//            $dateMon = $dateMon? $dateMon : 1;
//            $dateY = (int)$dataArr[2];
//            $createdDate = Carbon::createFromDate($dateY, $dateMon, $dateD);
//            $this->data['parsedDate'] = $createdDate;
//        } catch (Exception $e) {}
    }

    public function categories()
    {
        //remove first menu element
        $this->crawler->filter(self::$crawlerFilter['categoriesRemoveRoot'])->each(function (Crawler $nodes, $i)
        {
            foreach ($nodes as $node)$node->parentNode->removeChild($node);
        });

        $categories = $this->crawler->filter(self::$crawlerFilter['categories'])->each(function (Crawler $node, $i)
        {
            $href = trim($node->attr('href'));
            $text = trim($node->text());

            return [
                'href' => $href,
                'text' => $text,
            ];
        });

        $this->data['categories'] = $categories;

        if(empty($categories)) throw new ParseException('empty categories');
    }

    public function photos()
    {
        $items = $this->crawler->filter(self::$crawlerFilter['photos'])->each(function (Crawler $node, $i)
        {
            return $node->attr('src');
        });

        $this->data['photos'] = $items;
    }

    public function saveCategories()
    {
        $postCats = Category::parseCategory($this->data['categories']);
        $this->insertPostCats = array_pluck($postCats, 'id');

        if(empty($this->insertPostCats)) throw new ParseException('empty categories post save');
    }

    public function savePostCategories()
    {
        try {
            PostCategory::insert($this->insertPostCats);
        }catch (Exception $e){}
    }

    public function saveRegion()
    {
        $regions = Region::parseRegion($this->data['regionParsed']);
        $this->insertPostRegions = array_pluck($regions, 'id');

        if(empty($this->insertPostRegions)) throw new ParseException('empty regions post save');
    }

    public function savePostRegion ()
    {
        try {
            PostRegion::insert($this->insertPostRegions);
        }catch (Exception $e){}
    }

    public function specs ()
    {
        $items1Data = [];
        $items11Data = [];
        $items2Data = [];
        $items22Data = [];

        try {
            $items1 = $this->crawler->filter(self::$crawlerFilter['specs'])->eq(0);
            $items1Data = $items1->filter('ul')->eq(0)->filter('li')->each(function (Crawler $node, $i)
            {
                $key = $node->filter('.adPage__content__features__key')->text();
                $value = $node->filter('.adPage__content__features__value')->text();

                return [
                    'key' => trim($key),
                    'value' => trim(RegExp::replaceSpacesToOne($value)),
                ];
            });

        } catch (Exception $e) {}

        try {
            $items1 = $this->crawler->filter(self::$crawlerFilter['specs'])->eq(0);
            $items11Data = $items1->filter('ul')->eq(1)->filter('li')->each(function (Crawler $node, $i)
            {
                $key = $node->filter('.adPage__content__features__key')->text();
                $value = $node->filter('.adPage__content__features__value')->text();

                return [
                    'key' => trim($key),
                    'value' => trim(RegExp::replaceSpacesToOne($value)),
                ];
            });

        } catch (Exception $e) {}

        try {
            $items2 = $this->crawler->filter(self::$crawlerFilter['specs'])->eq(1);
            $items2Data = $items2->filter('ul')->eq(0)->filter('li')->each(function (Crawler $node, $i)
            {
                $key = $node->filter('.adPage__content__features__key')->text();
                $value = $node->filter('.adPage__content__features__value')->text();

                return [
                    'key' => trim($key),
                    'value' => trim(RegExp::replaceSpacesToOne($value)),
                ];
            });

        } catch (Exception $e) {}

        try {
            $items2 = $this->crawler->filter(self::$crawlerFilter['specs'])->eq(1);
            $items22Data = $items2->filter('ul')->eq(1)->filter('li')->each(function (Crawler $node, $i)
            {
                $key = $node->filter('.adPage__content__features__key')->text();
                $value = $node->filter('.adPage__content__features__value')->text();

                return [
                    'key' => trim($key),
                    'value' => trim(RegExp::replaceSpacesToOne($value)),
                ];
            });

        } catch (Exception $e) {}

        $this->data['specs'] = array_merge($items1Data, $items11Data, $items2Data, $items22Data);
    }

    public function saveSpecs()
    {
        if (empty($this->data['specs'])) {
            return;
        }

        $specs = Spec::parse($this->data['specs']);
        $this->insertPostSpecs = array_pluck($specs, 'id');
    }

    public function validatePost()
    {
        if (stripos($this->raw, 'Данное объявление недоступно') !== false) {
            throw new NotValidException('post is not valid');
        }
    }


    public function parseHtml()
    {
        if(!$this->raw)return false;

        try {
            $this->validatePost();
            $this->title();
            $this->text();
            $this->phone();
            $this->type();
            $this->shows();
            $this->region();
            $this->price();
            $this->authorName();
            $this->authorPath();
            $this->date();
            $this->photos();
            $this->categories();
            $this->saveCategories();
            $this->saveRegion();
            $this->specs();
            $this->saveSpecs();

            $setedPost = PostParsedNew::createPost($this->post, $this->dataToParsedPosts());
            if ($setedPost instanceof PostParsedNew) {
                //was parsed post
                $post = $this->post;
                $post->was_parsed = Post::WAS_PARSED_SUCCESS;
                $post->save();
            }

            return $setedPost;
        } catch (NotValidException $e) {
            $this->post->was_parsed = Post::WAS_PARSED_NOT_VALID;
            $this->post->save();
            Error::log($e, ['post_id' => $this->post->id, 'ad_id' => $this->post->_id]);
        } catch (Exception $e) {
            $this->post->was_parsed = Post::WAS_PARSED_ERROR;
            $this->post->save();
            Error::log($e, ['post_id' => $this->post->id, 'ad_id' => $this->post->_id]);
        }
    }

    public function dataToParsedPosts()
    {
        return [
            'created_at' => $this->post->created_at,
            'ad_id' => (int)$this->post->ad_id,
            'title' => $this->data['title'],
            'text' => $this->data['text'] ?? '',
            'search_text' => $this->data['searchText'] ?? '',
            'phones' => $this->data['phone'] ?? '',
            'type' => $this->data['type'],
            'shows' => (int)$this->data['shows'],
            'price' => $this->data['price'],
            'mdl' => $this->data['mdl'] ?? 0,
            'euro' => $this->data['euro'] ?? 0,
            'usd' => $this->data['usd'] ?? 0,
            'author' => $this->data['authorName'] ?? '',
            'author_path' => $this->data['authorPath'] ?? '',
            'photos' => $this->data['photos'] ?? '',
            'cat_ids' => $this->insertPostCats,
            'region_ids' => $this->insertPostRegions,
            'specs_ids' => $this->insertPostSpecs,
        ];
    }
}
