<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Proxy extends Model
{
    protected $fillable = ['ip', 'port', 'region', 'active'];

    public static function getActiveRandProxy()
    {
        return self::whereActive(1)->inRandomOrder()->first();
    }

    public function disableProxy()
    {
        $this->active = 0;
        $this->save();
        return $this;
    }

    public function enableProxy()
    {
        $this->active = 1;
        $this->save();
        return $this;
    }
}
