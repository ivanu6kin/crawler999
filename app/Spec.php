<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Spec extends Model
{
    public $timestamps = false;

    protected $fillable = ['key','value'];

    public static function getSpec($key, $value)
    {
        return static::where('key', '=', $key)->where('value', '=', $value)->first();
    }

    public static function parse(array $cats = [])
    {
        $return = [];

        foreach ($cats as $cat)
        {
            $key = $cat['key'];
            $value = $cat['value'];

            if(!$finded = self::getSpec($key, $value))
            {
                $finded = self::create(['key' => $key, 'value' => $value]);
            }

            $return[] = $finded->toArray();
        }

        return $return;
    }
}
