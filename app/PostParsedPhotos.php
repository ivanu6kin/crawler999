<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PostParsedPhotos extends Model
{
    protected $primaryKey = 'ad_id';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'ad_id',
        'path',
    ];
}
