<?php namespace App\RegExp;

class RegExp
{
    const ONLY_LETTERS_AND_DIGITS = '/[^a-zA-Zа-яА-Я0-9]/ui';
    const GET_DOLLARS = '/([0-9\.\,]+\s*)+[\$]{1}/u';
    const GET_EURO = '/([0-9\.\,]+\s*)+[\€]{1}/u';
    const GET_MDL = '/([0-9\.\,]+\s*)+[mdl|леев|лей|лея|leu|lei]{3}/u';
//    const MULTI_SPACE = '/(^\s+)|\s+/u';
//
//    public static function findAndReplaceByRegExp(array $regExpArr, $replace, $string)
//    {
//        foreach ($regExpArr as $search) {
//            $string = trim(str_replace($search, $replace, $string));
//        }
//
//        return $string;
//    }

    public static function clearTextOnlyLettersAndDigitns($string = '')
    {
        if(!$string)return '';
        return  mb_strtolower(preg_replace(self::ONLY_LETTERS_AND_DIGITS, '', $string));
    }

    public static function onlyMoney($string = '')
    {
        return preg_replace("/[^0-9\,\.]/","", $string);
    }

    public static function onlyInts($string = '')
    {
        return preg_replace("/[^0-9]/","", $string);
    }

    public static function getOnlyFirstInt($string = '')
    {
        $string = str_replace(' ', '', $string);
        $finded = preg_match('/:(\d+)/i', $string, $matches);
        if($finded)return $matches[0];

        return 0;
    }

    public static function getDollars($string = '')
    {
        $finded = preg_match(self::GET_DOLLARS, $string, $matches);
        if($finded)return $matches[0];

        return '';
    }

    public static function getEuro($string = '')
    {
        $finded = preg_match(self::GET_EURO, $string, $matches);
        if($finded)return $matches[0];

        return '';
    }

    public static function getMDL($string = '')
    {
        $finded = preg_match(self::GET_MDL, mb_strtolower($string), $matches);
        if($finded)return $matches[0];

        return '';
    }

    public static function searchText($string = '')
    {
        if(!$string)return '';

        return mb_strtolower(preg_replace(self::ONLY_LETTERS_AND_DIGITS, '', $string));
    }

    public static function replaceSpacesToOne($string = '')
    {
        if(!$string)return '';

        return preg_replace('!\s+!', ' ', $string);
    }

    public static function replaceSpaces($string = '')
    {
        if(!$string)return '';

        return preg_replace("/\s+/", '', $string);
    }
}
