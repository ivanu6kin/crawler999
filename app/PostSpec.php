<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostSpec extends Model
{
    public $timestamps = false;
    protected $fillable = ['post_id','spec_id'];

}
