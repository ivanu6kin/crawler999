<?php

namespace App\Console;

use App\Console\Commands\ClearTablesCommand;
use App\Console\Commands\NotificationBotCommand;
use App\Parser\Parser;
use App\Parser\PullFromPage;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

set_time_limit(0);

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Pull::class,
        Commands\PullFromPageCommand::class,
        Commands\GetProxies::class,
        Commands\ParsePost::class,
        Commands\ImportProxiesTxt::class,
        Commands\ParsePageNotPatsed::class,
        Commands\ParsePostToPieces::class,
        NotificationBotCommand::class,
        ClearTablesCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//         $schedule->command('pull:update')->everyMinute();
//         $schedule->command('reimport:parse:post')->everyFiveMinutes();
//         $schedule->command('proxy:get')->everyFiveMinutes();
//         $schedule->command('parse:post:pieces 400')->everyMinute();
         $schedule->command('notification:bot:command')->dailyAt('19:30');
         $schedule->command('clear:tables:command')->dailyAt('22:00');
            /**
             * php artisan pull:from:page - по 10 секунд парсинг добавили через crontab -e
             * проблема в sleep не работал
             */
        //get from pull and parse

        //set pull
        $schedule->call(function ()
        {
            $dt = Carbon::now();
            $x=6;
            do{
                $pull = resolve(PullFromPage::class);
                $pull->getPull();
                time_sleep_until($dt->addSeconds(10)->timestamp);
            } while($x-- > 0);

        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
