<?php

namespace App\Console\Commands;

use App\Parser\ParseProxies;
use App\Proxy;
use Illuminate\Console\Command;

class ImportProxiesTxt extends Command
{
    protected $signature = 'proxy:import:txt';

    protected $description = 'get proxies in database from txt';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $list = file_get_contents(app_path('Parser/proxyList.txt'));
        $arrayLists = explode("\n", $list);

        $exists = 0;
        $imported = 0;
        $this->info(sprintf('count proxies %s', count($arrayLists)));

        foreach ($arrayLists as $item)
        {
            try {
                $proxy = explode(':', $item);

                if(!$proxy[0] and !$proxy[1])
                {
                    $this->info(sprintf('error proxies %s : %s', $proxy[0], $proxy[1]));
                };

                if(!$findProxy = Proxy::whereIp($proxy[0])->first())
                {
                    Proxy::create(['ip' => $proxy[0], 'port' => $proxy[1], 'region' => 'txt']);
                    $imported++;
                }else{
                    $exists++;
                }

            } catch (\Exception $e)
            {
                $this->info(sprintf("Exception: %s", $e->getMessage()));
                continue;
            }
        }

        $this->info(sprintf("set proxies successful exists: %s, imported: %s", $exists, $imported));
    }
}
