<?php

namespace App\Console\Commands;

use App\NotificationBot\TelegramBot;
use Carbon\Carbon;
use Illuminate\Console\Command;

class NotificationBotCommand extends Command
{
    protected $signature = 'notification:bot:command';

    protected $description = 'Уведомить о текущем состоянии сервера';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $currentDayArray = [
            Carbon::now()->startOfDay()->toDateTimeString(),
            Carbon::now()->endOfDay()->toDateTimeString(),
        ];

        $todayPullsCount          = \App\Pull::whereBetween('created_at', $currentDayArray)->count();
        $todayPostsCount          = \App\Post::whereBetween('created_at', $currentDayArray)->count();
//        $todayPostParsedCount     = \App\PostParsedNew::whereBetween('created_at', $currentDayArray)->count();
        $todayPostParsedCount     = 0;
        $todayCategoryCount       = \App\Category::whereBetween('created_at', $currentDayArray)->count();
        $todayRegionCount         = \App\Region::whereBetween('created_at', $currentDayArray)->count();
        $todayErrorCount          = \App\Error::whereBetween('created_at', $currentDayArray)->count();
        $allSpecCount             = \App\Spec::count();
        $allPostParsedPhotosCount = \App\PostParsedPhotos::count();

        //last 100 posts
        $last100ParsedPostsWithoutData = [
            'ad_id' => 0,
            'title' => 0,
            'phones' => 0,
            'type' => 0,
            'shows' => 0,
            'price' => 0,
            'mdl' => 0,
            'euro' => 0,
            'usd' => 0,
            'author' => 0,
            'author_path' => 0,
            'photos' => 0,
        ];
        $todayLast100Posts = \App\PostParsedNew::orderBy('id', 'desc')
            ->limit(100)
            ->get()
            ->toArray();

        foreach ($todayLast100Posts as $todayLast100Post) {
            foreach ($last100ParsedPostsWithoutData as $withoutKey => $withoutCounter) {
                if (!key_exists($withoutKey, $todayLast100Post)) {
                    continue;
                }

                if (is_array($todayLast100Post[$withoutKey]) && empty($todayLast100Post[$withoutKey])) {
                    $last100ParsedPostsWithoutData[$withoutKey] = ++$last100ParsedPostsWithoutData[$withoutKey];
                    continue;
                }

                if(is_string($todayLast100Post[$withoutKey]) && empty(trim($todayLast100Post[$withoutKey]))) {
                    $last100ParsedPostsWithoutData[$withoutKey] = ++$last100ParsedPostsWithoutData[$withoutKey];
                }
            }
        }

        #linux output start
        $volumes_data = shell_exec('df -h');
        #linux output end

        /** @var TelegramBot $telegramBot */
        $telegramBot = resolve(TelegramBot::class);
        $telegramBot->notify([
            'current_day' => Carbon::now()->toDateString(),
            'pulls_count' => $todayPullsCount,
            'posts_count' => $todayPostsCount,
            'posts_parsed_new_count' => $todayPostParsedCount,
            'categories_count' => $todayCategoryCount,
            'regions_count' => $todayRegionCount,
            'specs_all_count' => $allSpecCount,
            'errors_count' => $todayErrorCount,
            'posts_parsed_photos_all_count' => $allPostParsedPhotosCount,
            '#########' => '#########',
            'last_100_without_data' => json_encode($last100ParsedPostsWithoutData),
            'volumes_data' => $volumes_data,
        ]);
        $this->info('Create pull successfully');
    }
}
