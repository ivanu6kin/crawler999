<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Parser\ParsePage;
use App\Post;

class ParsePostToPieces extends Command
{
    protected $signature = 'parse:post:pieces {limit}';

    protected $description = 'parse posts from raw';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $limit = $this->argument('limit');
        $this->info(sprintf('start parse posts to pieces limit %s', $limit));
        $postsRaw = Post::where('request_code', '=', 200)->where('was_parsed', '=', 0)->limit($limit)->get();

        foreach ($postsRaw as $post)
        {
            $postGet = new ParsePage($post, true);
            $postGet->parseHtml();

            $this->info(sprintf('parse post %s', $post->id));
        }

        $this->info(sprintf('end parse posts to pieces %s', $postsRaw->count()));
    }
}
