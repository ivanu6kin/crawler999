<?php

namespace App\Console\Commands;

use App\Parser\PullFromPage;
use Illuminate\Console\Command;

class PullFromPageCommand extends Command
{
    protected $signature = 'pull:from:page';
    protected $description = 'Update pull data for page';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        /** @var PullFromPage $pullFromPage */
        $pullFromPage = resolve(PullFromPage::class);
        $pullFromPage->getPull();
        $this->info('Create pull from page successfully');
    }
}
