<?php namespace App\Console\Commands;

use App\Parser\ParsePageImages;
use Illuminate\Console\Command;
use App\Parser\ParsePage;
use App\Post;
use \Exception;

class ParsePost extends Command
{
    protected $signature = 'parse:post {post}';

    protected $description = 'parse single post';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $postId = $this->argument('post');
        $this->info(sprintf('START parse post'));
        $post = Post::find($postId);
        if (!$post instanceof Post) {
            throw new Exception('no post id');
        }

        $parsedPost = new ParsePage($post, true);
        $postParsedNew = $parsedPost->parseHtml();

        /** @var ParsePageImages $service */
        $service = resolve(ParsePageImages::class);
        $service->saveImages($postParsedNew);

        $this->info($postParsedNew->ad_id);

        $this->info(sprintf('END parse post: %s', $postId));
    }
}
