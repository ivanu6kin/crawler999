<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClearTablesCommand extends Command
{
    protected $signature = 'clear:tables:command';
    protected $description = 'Remove old data from tables';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $timeSubDays = Carbon::now()->subDays(2);
        $pullsDb = DB::table('pulls')->whereDate('created_at', '<=', $timeSubDays);
        $postsDb = DB::table('posts')->whereDate('created_at', '<=', $timeSubDays);
        $errorDb = DB::table('error')->whereDate('created_at', '<=', $timeSubDays);

        $pullsDb->delete();
        $postsDb->delete();
        $errorDb->delete();

        $this->info('DELETEs TABLE DATA errors posts pulls');
    }
}
