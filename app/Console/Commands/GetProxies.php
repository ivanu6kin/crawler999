<?php

namespace App\Console\Commands;

use App\Parser\ParseProxies;
use Illuminate\Console\Command;

class GetProxies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'proxy:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get proxies in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * RUN COMMAND FROM TERMINAL
     * php artisan proxy:get
     *
     * @return mixed
     */
    public function handle()
    {
        $parse = new ParseProxies();
        $parse->getPageAndSave();
        $this->info("set proxies successful");
    }
}
