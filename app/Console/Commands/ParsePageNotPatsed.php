<?php namespace App\Console\Commands;
use App\Post;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ParsePageNotPatsed extends Command
{
    protected $signature = 'reimport:parse:post';

    protected $description = 'reimport all pages with errors and problems';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $to = Carbon::now()->subMinute(5)->toDateTimeString();
        $from = Carbon::now()->subHour(2)->toDateTimeString();

        $needReimportPosts = Post::whereBetween('created_at', [$from, $to])->get();

        $needReimportPosts->filter(function($post)
        {
            if(
                empty($post->raw) or
                strripos($post->raw, 'Please update your Google Chrome for the best experience') !== false or
                strripos($post->raw, 'Redirecting you to Web Security Gateway') !== false or
                strripos($post->raw, 'HTTP/1.1 400 Bad Request') !== false
            )
            {
                $post->raw = '';
                $post->save();

                $this->info("Post {$post->id} try reimport");

                //reparse
                exec("/usr/local/bin/php /var/www/html/artisan parse:post {$post->id} > /dev/null &");

                return $post;
            }
        });

        $this->info('reimport parse post');
        return;
    }
}
