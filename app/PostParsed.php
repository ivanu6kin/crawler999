<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PostParsed extends Model
{
    protected $fillable = [
        'post_id',
        'created_post',
        'ad_id',
        'title',
        'text',
        'search_text',
        'phones',
        'type',
        'shows',
        'price',
        'mdl',
        'euro',
        'usd',
        'author',
        'author_path',
        'photos',
        'duplicate',
        'parsed',
        'created_post',
    ];

    protected $attributes = [
        'text' => '',
        'photos' => '',
    ];

    public function parsedPhotos()
    {
        return $this->hasMany(PostParsedPhotos::class, 'ad_id', 'ad_id');
    }

    public static function findDublicate($text = '', $author = '')
    {
        if(!empty($text) and !empty($author))
        {
            return self::where('search_text', '=', $text)->where('author', '=', $author)->first();
        }

        return false;
    }

    public static function createPost(Post $post, array $array = [])
    {
        /**
         * Найти дубль по автору и поисковой строке
         */
        if(
            !empty($array['search_text']) &&
            !empty($array['author']) &&
            PostParsed::findDublicate($array['search_text'], $array['author'])
        ) {
            $array['duplicate'] = 1;
        };

        /**
         * Найти дубль по id из 999
         */
        if($findedDublicateByAdid = PostParsed::where('ad_id', $post->ad_id)->first()) {
            $array['duplicate'] = 1;
        }

        $findedParsedPost = PostParsed::where('post_id', $post->id)->first();
        if (!$findedParsedPost) {
            //not finded
            $data = array_merge(['post_id' => $post->id], $array);
            return self::create($data);
        }
        //set data new

        return $findedParsedPost;
    }
}
