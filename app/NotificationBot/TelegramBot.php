<?php namespace App\NotificationBot;

use GuzzleHttp\Client;

class TelegramBot
{
    private $client;

    private $url = 'https://api.telegram.org';
    /**
     * Телеграм токен получен через fatherbot
     * @var string
     */
    private $token = '1694936414:AAFcDRJBCtzDO0_pSM2xFn5AxelV1kG8d5g';

    /**
     * Список чатов получен через
     * https://api.telegram.org/bot1694936414:AAFcDRJBCtzDO0_pSM2xFn5AxelV1kG8d5g/getUpdates
     * @var array
     */
    private $chat_id_list = [
        '590636548', //kate
        '318925372', //ivan
    ];

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    private function buildText (array $arr = []): string
    {
        $text = '';
        foreach ($arr as $name => $value) {
            $text .= "{$name} = {$value} \n\r";
        }

        return $text;
    }

    public function notify (array $arr)
    {
        $apiUri = sprintf('%s/bot%s/%s', $this->url, $this->token, 'sendMessage');

        foreach ($this->chat_id_list as $chat_id) {
            $this->client->post($apiUri, [
                'form_params' => [
                    'chat_id' => $chat_id,
                    'text' => $this->buildText($arr),
                ],
            ]);
        }
    }
}
