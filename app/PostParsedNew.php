<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PostParsedNew extends Model
{
    protected $table = 'post_parsed_new';

    public $timestamps = ["created_at"];
    const UPDATED_AT = null;

    protected $casts = [
        'photos' => 'array',
        'specs_ids' => 'array',
        'cat_ids' => 'array',
        'region_ids' => 'array',
    ];

    protected $fillable = [
        'ad_id',
        'title',
        'type',
        'text',
        'search_text_hash',
        'phones',
        'shows',
        'price',
        'euro',
        'mdl',
        'usd',
        'author',
        'author_path',
        'photos',
        'specs_ids',
        'cat_ids',
        'region_ids',
        'is_first',
        'is_last',
    ];

    public static function generateSearchTextHash(string $text) {
        return md5($text);
    }

    public static function createPost(Post $post, array $array = [])
    {
        /**
         * Найти дубль по id из 999
         */
        if (self::where('ad_id', $post->ad_id)->first()) {
            $array['is_first'] = 0;
        }

        /**
         * prev posts is_last = 0
         */
        self::where('ad_id', '=', $post->ad_id)->update(['is_last' => 0]);

        /**
         * last post is_last = 1
         */
        $array['is_last'] = 1;

        $array['ad_id'] = $post->ad_id;
        $array['search_text_hash'] = self::generateSearchTextHash($array['search_text']);

        return self::create($array);
    }
}
