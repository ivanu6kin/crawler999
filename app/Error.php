<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Exception;

class Error extends Model
{
    protected $table = 'error';

    protected $fillable = [
        'post_id',
        'ad_id',
        'message',
        'file',
        'line',
        'category',
        'debug_backtrace',
    ];

    public static function log(Exception $e, array $data = [])
    {
        $arr = [
            'message'=> $e->getMessage(),
            'file' => $e->getFile(),
            'debug_backtrace' => $e->getTraceAsString(),
            'line' => $e->getLine(),
        ];

       $error = self::create(array_merge($arr, $data));
       $error->save();
    }
}
