<?php namespace App\Http\Controllers;

use App\Category;
use App\Parser\ParsePage;
use App\Post;
use App\PostParsed;
use App\Region;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class CrawlerController extends BaseController
{
    public function all (Request $request)
    {
        $posts = Post::orderBy('id', 'desc')->paginate(100);
        return view('account.all', ['posts' => $posts]);
    }

    public function allParsed (Request $request)
    {
        $posts = PostParsed::orderBy('id', 'desc')->paginate(100);
        return view('account.parsed', ['posts' => $posts]);
    }

    public function posts()
    {
        return view('account.posts');
    }

    public function filter (Request $request)
    {
        dd('need to ajax filter');
        //request
        $cats = $request->get('categories', null);
        $regions = $request->get('regions', null);
        $specs = $request->get('specs', null);

        //db
        $categories = Category::all();
        $db_regions = Region::select('id', 'name')->get()->toArray();
        $db_specs = Region::get()->toArray();

        $postsQuery = DB::table( 'post_parseds as pp' )->select(
            'pp.*'
        );

        //cats
        if(!empty($cats))
        {
            $postsQuery->join('post_categories as pc', 'pp.post_id', '=', 'pc.post_id');
            $postsQuery->whereIn('pc.cat_id', $cats);
        }

        //regions
        if(!empty($regions))
        {
            $postsQuery->join('post_regions as pr', 'pp.post_id', '=', 'pr.post_id');
            $postsQuery->whereIn('pr.region_id', $regions);
        }

        //specs
        if(!empty($specs)){
            $postsQuery->join('post_specs as ps', 'pp.post_id', '=', 'ps.post_id');
            $postsQuery->whereIn('ps.region_id', $specs);
        }

        if(!empty($cats) or !empty($regions) or !empty($specs))
        {
            $postsQuery->groupBy('pp.post_id');
        }

        //проблемы с created_at and group by
        $postsQuery->orderBy('pp.post_id', 'desc');
        $posts = $postsQuery->paginate(50);

        //new
//        $perPage = 50;
//        $page = $request->input("page", 1);
//
//        $skip = $page * $perPage;
//
//        $take = 5;
//
//        if($skip < 0) { $skip = 0; }
//
//
//        $totalCount = $postsQuery->select(DB::raw('count(pp.post_id)'))->toSql();
//        dd($totalCount);
//
//        $results = $postsQuery
//            ->take($perPage)
//            ->skip($skip)
//            ->get();
//
//        $paginator = new LengthAwarePaginator($results, $totalCount, $take, $page);
        //new

//        dd($posts);




//        $select

//        return static::from( 'user_referrals as ur' )
//            ->select(
//                'ur.parent_id',
//                'urm.user_id',
//                'urm.created_at',
//                \DB::raw('count(ur.parent_id) as totalRef'),
//                'u.fullname',
//                'u.email'
//            )
//            ->join('user_referrals as urm', 'urm.id', '=', 'ur.parent_id')
//            ->join('users as u', 'urm.user_id', '=', 'u.id')
//            ->OfEmail('u.email', $params['email'])
//            ->groupBy('ur.parent_id')
//            ->having('totalRef', '>', 0)
//            ->orderBy('u.created_at', 'desc')
//            ->paginate(50);

//        $posts = PostParsed::orderBy('id', 'desc')->paginate(100);

        return view('account.filter', [
            'posts' => $posts,
            'categories' => $categories,
            'specs' => $db_specs,
            'regions' => $db_regions,
            'filterData' => [
                'cats' => !empty($cats)? array_values($cats) : [],
                'regions' => !empty($regions)? array_values($regions) : [],
            ],
        ]);
    }

    public function index (Request $request)
    {

//        dd(Proxy::importProxyesTxt());

        $post = Post::find($request->get('post_id'));

        if(!$post instanceof Post)return 'false';

        $parse = new ParsePage($post, true);
        $parsed = $parse->parseHtml();

        dd($parsed);



//$postGet = new \App\Parser\ParsePage($post, true);
//$parsed = $postGet->parseHtml();
//dd($parsed);


////        $client = resolve(ParserClient::class);
////        dd($client);
//        $post = Post::find(11541);
//        $parse = new ParsePage($post, false);
//        dd($parse->parseHtml());
////        $parse = resolve(Parser::class);
////        $parse->parsePull(Pull::find(1));
////        dd(PostParsed::create(['post_id' => 1, 'created_post' => Carbon::now()]));
//        $post = Post::find(16);
//        $parser = new ParsePage($post);
//        dd($parser->parseHtml());
    }
}
