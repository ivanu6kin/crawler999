<?php namespace App\Http\Controllers;

use App\Category;
use App\PostParsed;
use App\Region;
use App\Spec;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class ApiController extends BaseController
{
    public function regions (Request $request)
    {
        $region = $request->get('region');
        if(mb_strlen($region) < 3) {
            return response(['error' => 'region lenght min 3'], 400);
        }

        $regions = Region::select('*')->where('name', 'like', '%' . $region . '%')->get();

        return response($regions);
    }

    public function specs (Request $request)
    {
        $spec = $request->get('spec');
        if(mb_strlen($spec) < 3) {
            return response(['error' => 'spec lenght min 3'], 400);
        }

        $specs = Spec::select('*')
            ->where('key', 'like', '%' . $spec . '%')
            ->orderBy('value', 'desc')
            ->get();

        return response($specs);
    }

    public function cat ()
    {
        return response(Category::all());
    }

    public function posts (Request $request)
    {
        //request
        $tags = $request->get('tags', []);
        $tagsRegion = $tags['region'];
        $tagsSpec = $tags['spec'];
        $tagsCat = $tags['cat'];
        $filterMoneyLess = $request->get('filter_money_less', '');
        $filterMoneyMore = $request->get('filter_money_more', '');
        $filterMoneyType = $request->get('filter_money_type', '');
        $sellType = $request->get('sell_type', '');
        $adId = $request->get('ad_id', null);
        $desc = $request->get('desc', null);
        $regionPart = $request->get('region_part', null);
        $title = $request->get('title', null);

        $postsQuery = PostParsed::with('parsedPhotos')
            ->from('post_parseds as pp')
            ->select( 'pp.*');

        if (!empty($adId)) {
            $postsQuery->where('pp.ad_id', '=', $adId);
        }

        if (!empty($sellType)) {
            $postsQuery->where('pp.type', '=', $sellType);
        }

        if (in_array($filterMoneyType, ['mdl', 'usd', 'euro'])) {
            if ($filterMoneyMore > 0) {
                $postsQuery->where(sprintf('pp.%s', $filterMoneyType), '>=', $filterMoneyMore);
            }

            if ($filterMoneyLess > 0) {
                $postsQuery->where(sprintf('pp.%s', $filterMoneyType), '<=', $filterMoneyLess);
            }
        }

        if (!empty($title)) {
            $postsQuery->where('pp.title', 'like', '%' . $title . '%');
        }

        if (!empty($desc)) {
            $postsQuery->where('pp.text', 'like', '%' . $desc . '%');
        }

        //cats
        if (!empty($tagsCat))
        {
            $postsQuery->join('post_categories as pc', 'pp.post_id', '=', 'pc.post_id');
            $postsQuery->whereIn('pc.cat_id', $tagsCat);
        }

        //regions
        $regions = [];
        if (!empty($regionPart)) {
            $regions = Region::select('id')
                ->where('name', 'like', '%' . $regionPart . '%')
                ->pluck('id')
                ->toArray();
        }

        if (!empty($regions) || !empty($tagsRegion)) {
            $postsQuery->join('post_regions as pr', 'pp.post_id', '=', 'pr.post_id');
            $postsQuery->whereIn('pr.region_id', array_unique(array_merge($tagsRegion, $regions)));
        }
        //regions end

        if (!empty($tagsSpec))
        {
            $postsQuery->join('post_specs as ps', 'pp.post_id', '=', 'ps.post_id');
            $postsQuery->whereIn('ps.spec_id', $tagsSpec);
        }

        if(!empty($tagsCat) or !empty($tagsRegion) or !empty($tagsSpec) or !empty($regions))
        {
            $postsQuery->groupBy('pp.ad_id');
        }

        //for single post remove group by
        if (!empty($adId)) {
            $postsQuery->groupBy('pp.id');
        }

        $postsQuery->orderBy('pp.id', 'desc');
        $sql = $postsQuery->toSql();
        $posts = $postsQuery->paginate(50);

        return response(['data' => $posts, 'sql' => $sql]);

//        if(!empty($tagsCat)) {
//
//        }
//


        //cats
        if(!empty($cats))
        {
            $postsQuery->join('post_categories as pc', 'pp.post_id', '=', 'pc.post_id');
            $postsQuery->whereIn('pc.cat_id', $cats);
        }

        //regions
        if(!empty($regions))
        {
            $postsQuery->join('post_regions as pr', 'pp.post_id', '=', 'pr.post_id');
            $postsQuery->whereIn('pr.region_id', $regions);
        }

        //specs
        if(!empty($specs)){
            $postsQuery->join('post_specs as ps', 'pp.post_id', '=', 'ps.post_id');
            $postsQuery->whereIn('ps.spec_id', $specs);
        }

        if(!empty($cats) or !empty($regions) or !empty($specs))
        {
            $postsQuery->groupBy('pp.post_id');
        }

        //проблемы с created_at and group by
        $postsQuery->orderBy('pp.post_id', 'desc');
        $posts = $postsQuery->paginate(50);

        //new
//        $perPage = 50;
//        $page = $request->input("page", 1);
//
//        $skip = $page * $perPage;
//
//        $take = 5;
//
//        if($skip < 0) { $skip = 0; }
//
//
//        $totalCount = $postsQuery->select(DB::raw('count(pp.post_id)'))->toSql();
//        dd($totalCount);
//
//        $results = $postsQuery
//            ->take($perPage)
//            ->skip($skip)
//            ->get();
//
//        $paginator = new LengthAwarePaginator($results, $totalCount, $take, $page);
        //new

//        dd($posts);




//        $select

//        return static::from( 'user_referrals as ur' )
//            ->select(
//                'ur.parent_id',
//                'urm.user_id',
//                'urm.created_at',
//                \DB::raw('count(ur.parent_id) as totalRef'),
//                'u.fullname',
//                'u.email'
//            )
//            ->join('user_referrals as urm', 'urm.id', '=', 'ur.parent_id')
//            ->join('users as u', 'urm.user_id', '=', 'u.id')
//            ->OfEmail('u.email', $params['email'])
//            ->groupBy('ur.parent_id')
//            ->having('totalRef', '>', 0)
//            ->orderBy('u.created_at', 'desc')
//            ->paginate(50);

//        $posts = PostParsed::orderBy('id', 'desc')->paginate(100);

        return view('account.filter', [
            'posts' => $posts,
            'categories' => $categories,
            'specs' => $db_specs,
            'regions' => $db_regions,
            'filterData' => [
                'cats' => !empty($cats)? array_values($cats) : [],
                'regions' => !empty($regions)? array_values($regions) : [],
            ],
        ]);
    }
}
