<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name','parent_id'];

    public static function getCategoryByName($name, $parent = 0)
    {
        return static::whereName($name)->whereParentId($parent)->first();
    }

    public static function parseCategory(array $cats = [])
    {
        $parent = 0;
        $return = [];

        foreach ($cats as $cat) {
            $catName = $cat['text'];
            //find from parent 0

            if(!$finded = self::getCategoryByName($catName, $parent))
            {
                $finded = self::create(['name' => $catName, 'parent_id' => $parent]);
            }
            $parent = $finded->id;
            $return[] = $finded->toArray();
        }

        return $return;
    }
}
