<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $fillable = ['name','parent_id'];

    public static function getCategoryByName($name, $parent = 0)
    {
        return static::whereName($name)->whereParentId($parent)->first();
    }

    public static function parseRegion(array $cats = [])
    {
        $parentRegion = 0;
        $return = [];

        foreach ($cats as $name)
        {
            //find from parent 0
            if(!$findedRegion = self::getCategoryByName($name, $parentRegion))
            {
                $findedRegion = self::create(['name' => $name, 'parent_id' => $parentRegion]);
            }

            $parentRegion = $findedRegion->id;
            $return[] = $findedRegion->toArray();
        }

        return $return;
    }

}
