var debounce = (func, wait, immediate) =>
{
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

async function load (url = '', type, data = {})
{
    type = type || 'GET';
    let requestData = {
        method: type,
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json'
        },
    };
    if(type === 'POST') {
        requestData['body'] = JSON.stringify(data);
    }

    try {
        const response = await fetch(url, requestData);
        const responseData = await response.json();
        if (responseData.error !== undefined) {
            console.log(`Validation Error: ${responseData.error}`);
            return [];
        }

        return responseData;
    } catch (e) {
        throw `request server error ${e}`;
    }
}
