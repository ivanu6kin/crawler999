new Vue({
    el: "#filter",
    data: {
        //filter inputs
        filter_regions: '',
        filter_spec: '',
        filter_cat: '',
        filter_title: '',
        filter_desc: '',
        // filter_type: '',
        filter_sell_type: '',
        filter_ad_id: '',
        filter_region_part: '',
        filter_money_type : '',
        filter_money_more : '',
        filter_money_less : '',
        // filter_mdl: '',
        // filter_usd: '',
        // filter_eur: '',
        filter_tags: {},
        //filtered data from server
        regions: [],
        specs: [],
        cats: [],
        //posts with paginate
        posts: [],
        pagination: [],
        is_loading: true,
    },
    delimiters: ["[","]"],
    filters: {
        jsonPretty: function(data) {
            // console.log(data);
            return JSON.stringify(data, null, 2)
        },
    },
    created: async function () {
        this.loadCats();
        this.loadPosts();
    },
    methods: {
        filterRegions: debounce( async function (e)
        {
            console.log('filterRegions: ', e.target.value);
            let data = await load(`/account/api/region/?region=${e.target.value}`);
            this.regions = data;
            console.log(data);
        }, 500),
        filterSpecs: debounce(async function (e)
        {
            console.log('filterSpecs: ', e.target.value);
            let data = await load(`/account/api/specs/?spec=${e.target.value}`);
            this.specs = data;
            console.log(data);
        }, 500),
        loadCats: async function (e)
        {
            console.log('loadCats');
            let data = await load(`/account/api/cat`);
            this.cats = data;
            console.log('loadCats', data);
        },
        async loadPosts() {
            let tags = {
                'region': [],
                'spec': [],
                'cat': [],
            };

            Object.keys(this.filter_tags).map((key) => {
                let tag = this.filter_tags[key];
                tags[tag.filter_type].push(tag.id);
            });

            this.is_loading = true;
            let data;
            try {
                data = await load(`/account/api/posts?page=${this.pagination.current}`, 'POST', {
                    'title': this.filter_title,
                    'ad_id': this.filter_ad_id,
                    'desc': this.filter_desc,
                    // 'type': this.filter_type,
                    'sell_type': this.filter_sell_type,
                    'region_part': this.filter_region_part,
                    'filter_money_type': this.filter_money_type,
                    'filter_money_more': this.filter_money_more,
                    'filter_money_less': this.filter_money_less,
                    'tags': tags,
                });
                this.is_loading = false;
            } catch (e) {
                this.is_loading = false;
                console.log(e);
                return;
            }

            this.posts = data.data.data;

            this.pagination = {
                'current': data.data.current_page,
                'total': data.data.total,
                'last_page': data.data.last_page,
            };

            console.log('load data', data.data);
            console.log('load sql', data.sql);
        },
        addRegionToFilterTags: function (region) {
            region['filter_type'] = 'region';
            this.filter_tags[this.generateTagId('region', region.id)] = region;
            this.filter_tags = Object.assign({}, this.filter_tags);
        },
        addSpecToFilterTags: function (spec) {
            spec['filter_type'] = 'spec';
            this.filter_tags[this.generateTagId('spec', spec.id)] = spec;
            this.filter_tags = Object.assign({}, this.filter_tags);
        },
        addCatToFilterTags: function (cat) {
            cat['filter_type'] = 'cat';
            this.filter_tags[this.generateTagId('cat', cat.id)] = cat;
            this.filter_tags = Object.assign({}, this.filter_tags);
        },
        deleteFilterTag: function (key) {
            delete this.filter_tags[key];
            this.filter_tags = Object.assign({}, this.filter_tags);
        },
        generateTagId (type, id) {
            if (!type) {
                throw 'type undefined';
            }

            if (!id) {
                throw 'id undefined';
            }

            return `${type}${id}`
        },
        showTag(tag)
        {
            switch (tag.filter_type) {
                case 'region':
                    return tag.name;
                case 'spec':
                    return `${tag.key}=${tag.value}`;
                case 'cat':
                    return tag.name;
                default:
                    throw 'filter_type not found'
            }
        },
        paginationChange(e) {
            if (e.target.value < 1) {
                this.pagination.current = 1;
            }

            if(parseInt(e.target.value) > this.pagination.last_page ) {
                this.pagination.current = this.pagination.last_page;
            }

            console.log('change', this.pagination.current);
        },
        getPhotoUrl(url) {
            return `/999/${url}`
        },
        getParsed999PhotoUrls(json)
        {
            return JSON.parse(JSON.parse(json));
        },
        async submit() {
            // this.pagination.current = 1;
            await this.loadPosts();
        }
    }
});
