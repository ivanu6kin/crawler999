const express = require('express');
const cron    = require('node-cron');
const log = require('simple-node-logger').createSimpleLogger();
const util = require( 'util' );
const mysql = require( 'mysql' );
const request = require('request');
const fs = require('fs');
const axios = require('axios');
const gm = require('gm');
require('dotenv').config();
console.log(require('dotenv').config());

const ENV = {
    port: process.env.PORT,
    host: process.env.HOST,

    db_host: process.env.DB_HOST,
    db_username: process.env.DB_USER,
    db_password: process.env.DB_PASS,
    db_database: process.env.DB_DATABASE,

    photos_path: process.env.PHOTOS_PATH,
    raw_posts_path: process.env.RAW_POSTS_PATH,
};

console.log('CREDENTIALS: ', ENV);

// константы
const port = ENV.port;
const host = ENV.host;
// приложение
const app = express();

function makeDb( config )
{
    const connection = mysql.createConnection( config );
    return {
        query( sql, args ) {
            return util.promisify( connection.query )
                .call( connection, sql, args );
        },
        close() {
            return util.promisify( connection.end ).call( connection );
        },
        async get( sql, args ) {
            try {
                let res = await util.promisify( connection.query ).call( connection, sql, args );
                if (res.length > 0) {
                    return JSON.parse(JSON.stringify(res));
                }
            } catch ( err ) {
                log.error(err);
            } finally {
                //await db.close();
            }

            log.warn('NO RESULT QUERY: ', sql);
            return null;
        },
    };
}

const db = makeDb({
    host     : ENV.db_host,
    user     : ENV.db_username,
    password : ENV.db_password,
    database : ENV.db_database
});

(async ()=> {
    cron.schedule('*/3 * * * * *', async () => {
        console.log('start parse images');
        //proxy
        let parsed_post = await db.get(`SELECT id, ad_id, photos, is_parsed_photos, created_at FROM post_parsed_new where is_parsed_photos = 0 order by id desc limit 1`);

        if (!parsed_post) {
            return;
        }

        parsed_post = parsed_post[0];
        console.log('start parse image', parsed_post.ad_id);

        let findedPostPhoto = await db.get(`SELECT ad_id FROM post_parsed_photos where ad_id = ${parsed_post.ad_id} limit 1`);
        //if exists return
        if (findedPostPhoto) {
            console.log('Image exist', parsed_post.ad_id);
            db.query(`UPDATE post_parsed_new set is_parsed_photos = 1 where ad_id = ${parsed_post.ad_id}`);
            return;
        }

        let photos = JSON.parse(parsed_post['photos']);
        photos = photos.slice(0, 3);
        let postDate = new Date(parsed_post.created_at);
        let dirYear = `${ENV.photos_path}${postDate.getFullYear()}`;
        let dirYearMonth = `${ENV.photos_path}${postDate.getFullYear()}/${postDate.getMonth()}`;
        let dirYearMonthDay = `${ENV.photos_path}${postDate.getFullYear()}/${postDate.getMonth()}/${postDate.getDate()}`;
        let dirYearMonthDayPostId = `${ENV.photos_path}${postDate.getFullYear()}/${postDate.getMonth()}/${postDate.getDate()}/${parsed_post.ad_id}`;
        let dirSavePath = `${ENV.photos_path}${postDate.getFullYear()}/${postDate.getMonth()}/${postDate.getDate()}/${parsed_post.ad_id}`;
        let dirDbSavePath = `${postDate.getFullYear()}/${postDate.getMonth()}/${postDate.getDate()}/${parsed_post.ad_id}`;

        function getImageName(post, url, index)
        {
            let imageExtension = url.substring(url.lastIndexOf("."));
            return `${post.ad_id}[${index}]${imageExtension}`
        }

        if (!photos.length) {
            db.query(`UPDATE post_parsed_new set is_parsed_photos = 1 where ad_id = ${parsed_post.ad_id}`);
        }

        if (!fs.existsSync(dirYear)) {
            fs.mkdirSync(dirYear);
        }

        if (!fs.existsSync(dirYearMonth)) {
            fs.mkdirSync(dirYearMonth);
        }

        if (!fs.existsSync(dirYearMonthDay)) {
            fs.mkdirSync(dirYearMonthDay);
        }

        if (!fs.existsSync(dirYearMonthDayPostId)) {
            fs.mkdirSync(dirYearMonthDayPostId);
        }

        photos.map((url, index) => {
            let ad_id = `${parsed_post.ad_id}`;

            gm(request(url))
                .quality(50)
                .resize(200, 200)
                .write(`${dirSavePath}/${getImageName(parsed_post, url, index)}`, function (err) {
                    if (!err) {
                        console.log(`Image downloaded and resized ${ad_id}`);
                        db.query(`INSERT INTO post_parsed_photos (ad_id, path) VALUES (${ad_id}, ${JSON.stringify(dirDbSavePath + '/' + getImageName(parsed_post, url, index))})`);
                        db.query(`UPDATE post_parsed_new set is_parsed_photos = 1 where ad_id = ${ad_id}`);
                    }else {
                        console.log(`Download image error ${JSON.stringify(err)}`);
                    }
                });
        });
    });
})();

app.get('/', (req, res) => {
    res.send('Hello World');
    log.info('Hello World');
});

app.listen(port, host);

console.log(`running on http://${host}:${port}`);
