<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostParsedNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_parsed_new', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ad_id')->index()->default(0);
            $table->string('title')->default('');
            $table->string('type')->default('');
            $table->text('text');
            $table->string('search_text_hash', 32)->default('');
            $table->string('phones')->default('');
            $table->integer('shows')->default(0);
            $table->string('price')->default('');
            $table->integer('euro')->default(0);
            $table->integer('mdl')->default(0);
            $table->integer('usd')->default(0);
            $table->string('author')->default('');
            $table->string('author_path')->default('');
            $table->json('photos');
            $table->json('specs_ids');
            $table->json('cat_ids');
            $table->json('region_ids');
            $table->smallInteger('is_parsed_photos')->index()->default(0)->comment('not parsed = 0, parsed with photos = 1'); //можно соединить вместе 3 кнстанты not parsed, parsed, parsed_with_photos
            $table->smallInteger('is_first')->default(1)->comment('первый загруженный пост с сайта');
            $table->smallInteger('is_last')->default(0)->comment('последний загруженный пост с сайта');
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_parsed_new');
    }
}
