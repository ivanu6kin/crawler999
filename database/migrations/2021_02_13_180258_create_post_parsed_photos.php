<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostParsedPhotos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_parsed_photos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ad_id')->index();
            $table->string('path', 70)
                ->index()
                ->comment('example: 2020_02_02_213123131321321132131231231[1].jpeg');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_parsed_photos');
    }
}
