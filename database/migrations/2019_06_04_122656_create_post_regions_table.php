<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_regions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('post_id')->index()->unsigned();
            $table->integer('region_id')->index()->unsigned();

            $table->unique(['post_id', 'region_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_regions');
    }
}
