<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostParsedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_parseds', function (Blueprint $table)
        {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('post_id');
            $table->integer('ad_id')->index()->default(0);

            $table->string('title')->index()->default('');
            $table->text('text');
            $table->string('search_text', 400)->index()->default('');
            $table->string('phones')->default('');
            $table->string('type')->index()->default('');
            $table->integer('shows')->index()->default(0);

            $table->string('price')->default('');

            $table->integer('mdl')->index()->default(0);
            $table->integer('euro')->index()->default(0);
            $table->integer('usd')->index()->default(0);

            $table->string('author')->index()->default('');
            $table->string('author_path')->default('');

            $table->text('photos');

            $table->smallInteger('duplicate')->index()->default(0);
            $table->smallInteger('parsed')->index()->default(0);
            $table->dateTime('created_post');

            $table->timestamps();

            $table->foreign('post_id')->references('id')->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_parseds');
    }
}
