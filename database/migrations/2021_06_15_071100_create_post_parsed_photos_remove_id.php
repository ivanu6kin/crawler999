<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostParsedPhotosRemoveId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post_parsed_photos', function (Blueprint $table) {
            $table->dropColumn('id');
            $table->dropIndex('post_parsed_photos_path_index');
        });
    }

    public function down()
    {

    }
}
