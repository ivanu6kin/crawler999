<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table)
        {
            $table->bigIncrements('id');

            //pull data
            $table->bigInteger('pull_id')->default(0);
            $table->string('cat')->index()->default('');
            $table->string('sub_cat')->index()->default('');
            $table->string('cat_path')->default('');
            $table->string('sub_cat_path')->default('');
            $table->string('image')->default('');
            $table->string('title')->default('');
            $table->bigInteger('_id')->default(0);
            $table->string('ad_id')->default('');
            $table->string('type')->default('');
            $table->text('raw');
            $table->integer('request_code')->default(0);
            //pull data

//            //job
//            //parse data
//            $table->text('raw');
//            $table->integer('request_code')->default(0);
//            $table->string('phones')->default(0);
//
//            $table->integer('mdl')->default(0);
//            $table->integer('euro')->default(0);
//            $table->integer('usd')->default(0);
//
//            //from text
//            $table->integer('mdl_some')->default(0);
//            $table->integer('euro_some')->default(0);
//            $table->integer('usd_some')->default(0);
//
//            $table->string('author')->default('');
//            $table->string('author_path')->default('');
//
//            $table->text('text');
//            $table->string('search_text', 700)->index()->default('');
//
//            $table->text('photos');
//            $table->text('category');
//
//            $table->integer('duplicate')->index()->default(0);
//
//            //подумать
//            $table->string('regionRaw')->default('');
//            $table->string('region')->default('');
//            $table->string('country')->default('');
//            $table->string('city')->default('');
//
//            $table->integer('parsed')->default(0);
//            //parse data

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
