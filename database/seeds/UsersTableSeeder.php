<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'ivan',
            'email' => 'ivanu6kin@gmail.com',
            'password' => bcrypt('gogi100500'),
        ]);
    }
}
