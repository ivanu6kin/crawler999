#!/bin/sh

DIR=/var/www/dump
DUMP_FILE=${DIR}/mysql_`date +%m-%d-%Y`.sql.gz
LEAVE=$1
COUNTER=1

#dump
#mysqldump crawler | gzip > ${DUMP_FILE}
mysqldump -u root -p crawler | gzip > ${DUMP_FILE}
#remove old files
if ! [ -z "$LEAVE" ]; then
  for i in $(find ${DIR} -type f | sort -rn); do # Not recommended, will break on whitespace
    if [ "$LEAVE" -lt "$COUNTER" ]; then
        rm $i
    fi

    COUNTER=$((COUNTER+1))
  done
fi

#mysqldump -u root -p crawler | gzip > /var/www/dump/mysql_`date +%m-%d-%Y`.sql.gz
#/mnt/dump
#now create cron script smth like this
#crontab -e
#30 15 * * * /root/MySQLdump.sh 2>&1>> /root/MySQLdump.log
#The above will dump the database every day at 15:30.

#http://dbperf.wordpress.com/2010/06/11/automate-mysql-dumps-using-linux-cron-job/
